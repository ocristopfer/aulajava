import java.util.Scanner;

import javax.swing.JOptionPane;

public class ExerciciosEmAula {
	
	
	public void calcularmedia()
{
	double nota1, nota2, nota3, media;
	
	//Entrada de dados
			
	nota1 = capturanota("Insira a nota da AV1 (de 0 a 10) ");
	nota2 = capturanota("Insira a nota da AV2 (de 0 a 10) ");
	nota3 = capturanota("Insira a nota da AV3 (de 0 a 10) ");

	//Processamentos de dados
	media = (nota1 + nota2 + nota3)/3;
			
	//Saida de informa��o.
	if (media >= 7 )
	{
		
		JOptionPane.showMessageDialog(null, "Sua m�dia �: " + String.format("%.2f", media) +  System.lineSeparator()+ " Voc� est� aprovado" );
	}else
	{
		JOptionPane.showMessageDialog(null, "Sua m�dia �: " + String.format("%.2f", media) + System.lineSeparator() +" Voc� est� reprovado" );
		
	}
}

	public static double capturanota(String mensagem)
	{
		double nota = 0;
		try
		{
			nota = Double.parseDouble(JOptionPane.showInputDialog(mensagem + " : ").replace(",", "."));
		}
		catch(NumberFormatException ex)
		{
			JOptionPane.showMessageDialog(null, "O valor deve ser num�rico." );
			capturanota(mensagem);
		}
		catch(Exception ex)
		{
			
		}	
				
		while(nota > 10)
			
		{
			JOptionPane.showMessageDialog(null, "O valor maximo que pode ser informado � 10." );
			nota = Double.parseDouble(JOptionPane.showInputDialog(mensagem + " : ").replace(",", "."));
		}
		
		return nota;
		
		
	}

	public void calcular2valores()
	{
		int a=0,b=0;
		float result;
		Scanner teclado = new Scanner(System.in);
		System.out.println("digite um valor inteiro"  );
		a = teclado.nextInt();
				
		while (b == 0 )
		{
			System.out.println("digite um valor inteiro"  );
			b = teclado.nextInt();
		}
		result = a/b;
		System.out.println("O valor foi: " + result  );
		
	}
	
}
