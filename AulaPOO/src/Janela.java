import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Janela extends JFrame implements ActionListener {

    JButton B0, B1, B2, B3, B4, B5,B6,B7;
    boolean novajanela;
    int listadeexercicio;
 	
    public static void main(String args[])
    {
	    JFrame Janela= new Janela(false);
	    Janela.setVisible(true);
	     WindowListener x = new WindowAdapter()
	     {
	     public void windowClosing(WindowEvent e)
	      {
	       System.exit(0);
	      }
	     };
	    Janela.addWindowListener(x);
	  }

    
	public Janela(boolean novajanela)
	{
		this.novajanela = novajanela;
		 setTitle("Exercicios Java");
	     setSize(800,600);
	     getContentPane().setBackground(new Color(150,150,150));
	     getContentPane().setLayout(new GridLayout(0,2,5,5));
     
	     

	   if (novajanela == false)
		{
		 B0 = new JButton ("Exercicios em Aula");    
		 B0.addActionListener(this);
		 getContentPane().add(B0);
		     
	     B1 = new JButton ("Lista Simples");    
	     B1.addActionListener(this);
	     getContentPane().add(B1);
	     
	     B2 = new JButton ("Lista Sele��o");     
	     B2.addActionListener(this);
	     getContentPane().add(B2);
		}

	    
	}
	public void exercicios_em_aula()
	{

		JFrame Janela= new Janela(true);
	    Janela.setVisible(true);
	    novajanela = true;
	    listadeexercicio = 0;
        B1 = new JButton ("Calcular Media");      
        B1.addActionListener(this);
        Janela.getContentPane().add(B1);
        
		
	}
	public void exercicios_simples()
	{

		JFrame Janela= new Janela(true);
	    Janela.setVisible(true);
	    novajanela = true;
	    listadeexercicio = 1;
        B1 = new JButton ("Cacular Raio");      
        B1.addActionListener(this);
        B2 = new JButton ("Converte Fahrenheit para Celsius");      
        B2.addActionListener(this);
        B3 = new JButton ("Converte Celsius para Fahrenheit");    
        B3.addActionListener(this);

        Janela.getContentPane().add(B1);
        Janela.getContentPane().add(B2);
        Janela.getContentPane().add(B3);      
		
	}
	
	public void exercicios_selecao()
	{

		JFrame Janela= new Janela(true);
	    Janela.setVisible(true);
	    novajanela = true;
	    listadeexercicio = 2;
        B1 = new JButton ("Ler C�digo Produto");      
        B1.addActionListener(this);
        B2 = new JButton ("M�dia do aluno, com parametros");      
        B2.addActionListener(this);
        B3 = new JButton ("Ler notas e retornar parab�ns");    
        B3.addActionListener(this);

        Janela.getContentPane().add(B1);
        Janela.getContentPane().add(B2);
        Janela.getContentPane().add(B3);      
		
	}


	public void actionPerformed(ActionEvent e)
	 {
		
		if (e.getSource() == B0)
		{
			exercicios_em_aula();
		}
			
		if (e.getSource() == B1)
		{
			if (novajanela == false)
			{
				exercicios_simples();
			}
			else
			{
				switch (listadeexercicio)
				{
				case 0:
					ExerciciosEmAula Exec0 = new ExerciciosEmAula();
					Exec0.calcularmedia();
					break;
				case 1: 
					ExerciciosSimples Exec1 = new ExerciciosSimples();
					Exec1.tarefa1_calcularraio();
					break;
				case 2:
					ExerciciosSelecao Exec2 = new ExerciciosSelecao();
					Exec2.tarefa1_lercodigo();
					break;
				}

			}
		}
		if (e.getSource() == B2)
		{
			if (novajanela == false)
			{
				exercicios_selecao();
			}
			else
			{
				if(listadeexercicio == 1)
				{
					ExerciciosSimples Exec = new ExerciciosSimples();
					Exec.tarefa2_ftoc();
				}
				else
				{
						
				}
			}
		}
		if (e.getSource() == B3)
		{
			if (listadeexercicio == 1)
			{
				ExerciciosSimples Exec = new ExerciciosSimples();
				Exec.tarefa3_ctof();
			}
		}
	  
	 }
}
